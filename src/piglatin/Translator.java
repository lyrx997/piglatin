package piglatin;

public class Translator {
	
	private String inputPhrase;
	public static final String EMPTYPHRASE = "nil";

	public Translator(String inputPhrase) {
		
		this.inputPhrase = inputPhrase;
		
	}

	public String getPhrase() {
		
		return this.inputPhrase;
		
	}

	public String Translate() {
		
		if ( phraseContainsPunctuations() ) {
			
			return translatePhraseWithPunctuactions();
			
		}
		
		if ( inputPhrase.contains(" ") || inputPhrase.contains("-") ) {
			
			return translateSeparatedString();
			
		}
		
		else if ( startsWithVowel() ) {
			
			return translateVowelWord();
		
		} else if ( !(inputPhrase.equals("")) ) {
			
			return translateConsonantWord();
			
		}
		
		// empty inputPhrase
		return EMPTYPHRASE;
	}
	
	private String translateVowelWord() {
		
		if ( inputPhrase.endsWith("y") ) {
			
			return this.inputPhrase + "nay";
			
		} 
			else if ( endsWithVowel() ) {
			
			return this.inputPhrase + "yay";
			
		}
			else if ( !(endsWithVowel()) ) {
				
			return this.inputPhrase + "ay";
		}
		
		return null;
	}
	
	private String translateConsonantWord() {
		
		int i = 0;
		StringBuilder appendConsonant = new StringBuilder();
		while ( !(charIsVowel(i)) ) {
			
			appendConsonant.append(inputPhrase.charAt(i));
			
			i++;
		}
		return inputPhrase.substring(i) + appendConsonant.toString() + "ay";
		
	}
	
	private String translateSeparatedString() {
		
		String separator;
		
		if ( inputPhrase.contains(" ") ) {
			separator = " ";
		} else separator = "-";
		
		String[] words = inputPhrase.split(separator);
		
		StringBuilder translationOutput = new StringBuilder();
		
		for ( int i = 0; i < words.length; i++ ) {
			
			if ( !(words[i].equals("")) ) {
				
				Translator t = new Translator(words[i]);
				
				translationOutput.append(t.Translate());
				
				if (i + 1 != words.length) {
					
					translationOutput.append(separator);
					
				}
						
			}
		}
		
		return translationOutput.toString();
		
	}
	
	private String translatePhraseWithPunctuactions() {
		
		StringBuilder translatedPhrase = new StringBuilder();
		
		int i = 0;
		
		while ( i < inputPhrase.length() ) {
			
			StringBuilder temp = new StringBuilder();
			
			while ( !(phraseCharIsPunctuaction(i)) && i + 1 < inputPhrase.length()) {
				
				temp.append(inputPhrase.charAt(i));
				i++;
				
			}
			
			Translator t = new Translator(temp.toString());
			
			
			if ( i != inputPhrase.length() ) {
				translatedPhrase.append(t.Translate());
				translatedPhrase.append(inputPhrase.charAt(i));
				i++;	
			}

		}
		
		return translatedPhrase.toString();
	}
	
	private boolean startsWithVowel() {
		
		return ( inputPhrase.startsWith("a") || inputPhrase.startsWith("e") || inputPhrase.startsWith("i") 
				|| inputPhrase.startsWith("o") || inputPhrase.startsWith("u") );
	}
	
	private boolean endsWithVowel() {
		
		return ( inputPhrase.endsWith("a") || inputPhrase.endsWith("e") || inputPhrase.endsWith("i") 
				|| inputPhrase.endsWith("o") || inputPhrase.endsWith("u") );
	}
	
	private boolean charIsVowel(int i) {
		return inputPhrase.charAt(i) == 'a' || inputPhrase.charAt(i) == 'e' || inputPhrase.charAt(i) == 'i'
				|| inputPhrase.charAt(i) == 'o' || inputPhrase.charAt(i) == 'u';
	}
	
	private boolean phraseContainsPunctuations() {
		
		return (inputPhrase.contains(".") || inputPhrase.contains(",") || inputPhrase.contains(";") 
				|| inputPhrase.contains(":") || inputPhrase.contains("!") || inputPhrase.contains("?")
				|| inputPhrase.contains("'") || inputPhrase.contains("(") || inputPhrase.contains(")"));
		
	}
	
	private boolean phraseCharIsPunctuaction(int i) {
		return (inputPhrase.charAt(i) == '.' || inputPhrase.charAt(i) == ',' || inputPhrase.charAt(i) == ';' 
				|| inputPhrase.charAt(i) == ':' || inputPhrase.charAt(i) == '!' || inputPhrase.charAt(i) == '?'
				|| inputPhrase.charAt(i) == '\'' || inputPhrase.charAt(i) == '(' || inputPhrase.charAt(i) == ')');
	}

}
