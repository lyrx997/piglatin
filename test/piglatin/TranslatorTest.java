package piglatin;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		
		Translator t = new Translator("hello world");
		
		assertEquals( "hello world", t.getPhrase() );
		
	}
	
	@Test
	public void testEmptyPhraseTranslation() {
		
		Translator t = new Translator("");
		
		assertEquals( Translator.EMPTYPHRASE, t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithAAndEndingWithY() {
		
		Translator t = new Translator("any");
		
		assertEquals("anynay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndingWithY() {
		
		Translator t = new Translator("utility");
		
		assertEquals("utilitynay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndingWithVowel() {
		
		Translator t = new Translator("apple");
		
		assertEquals("appleyay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndingWithConsonant() {
		
		Translator t = new Translator("ask");
		
		assertEquals("askay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithConsonant() {
		
		Translator t = new Translator("hello");
		
		assertEquals( "ellohay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithTwoOrMoreConsonants() {
		
		Translator t = new Translator("known");
		
		assertEquals( "ownknay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithTwoWordsSpaceSeparated() {
		
		Translator t = new Translator("hello world");
		
		assertEquals( "ellohay orldway", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithTwoWordsCommaSeparated() {
		
		Translator t = new Translator("well-being");
		
		assertEquals( "ellway-eingbay", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingExclamationMark() {
		
		Translator t = new Translator("well-being!");
		
		assertEquals( "ellway-eingbay!", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingQuestionMark() {
		
		Translator t = new Translator("well-being?");
		
		assertEquals( "ellway-eingbay?", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingPoint() {
		
		Translator t = new Translator("well-being.");
		
		assertEquals( "ellway-eingbay.", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingComma() {
		
		Translator t = new Translator("well-being,");
		
		assertEquals( "ellway-eingbay,", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingSemiColon() {
		
		Translator t = new Translator("well-being;");
		
		assertEquals( "ellway-eingbay;", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingColon() {
		
		Translator t = new Translator("well-being:");
		
		assertEquals( "ellway-eingbay:", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithEndingRoundParenthesis() {
		
		Translator t = new Translator("well-being()");
		
		assertEquals( "ellway-eingbay(nil)", t.Translate() );
		
	}
	
	@Test
	public void testTranslationPhraseWithApostrophe() {
		
		Translator t = new Translator("don't");
		
		assertEquals( "onday'nilt", t.Translate() );
		
	}
	
	@Test
	public void testPhraseTranslationWithSpacesAndPunctuaction() {
		
		Translator t = new Translator("hello world!");
		
		assertEquals( "ellohay orldway!", t.Translate() );
		
	}

}
